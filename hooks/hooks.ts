import { BeforeAll, After, AfterAll, Status, setDefaultTimeout } from 'cucumber';
import { browser } from 'protractor';

BeforeAll({ timeout: 10000 }, async () => {
  await browser.waitForAngularEnabled(false);
  await setDefaultTimeout(70000);
});

AfterAll({ timeout: 100000 }, async function () {
  await browser.sleep(30000);
  await browser.quit();
});

After({}, async function (scenario) {
  if (scenario.result.status === Status.FAILED) {
    const screenShot = await browser.takeScreenshot();
    await this.attach(screenShot, 'image/png');
  }
  

})


