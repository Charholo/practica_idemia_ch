import { Config, browser } from "protractor";
import { writeFile } from "fs";
import { build$ } from 'protractor/built/element';

var report_file_name:string = "";
var path = require('path');
var downloadsPath = path.resolve(__dirname, './');

export const config: Config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',

    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    specs: ['../../features/*.feature'], 

    //Asociar los .feature creados previamente en la ruta features
    suites:{
        "Login": "../../features/Login.feature"
    },

    params: {
        "urlPortal": "https://userinyerface.com"
    },

    capabilities: {
        "browserName": "chrome",
        "chromeOptions": {

            prefs: {
                'downloads': {
                    'prompt_for_download': false,
                    'directory_upgrade': true,
                    'default_directory': downloadsPath
                }
            }
        },
        "name": "Test_Automate",
        "build": "UserInyerface",
        "idleTimeout": 90,
        "screenResolution": "1920x1080"
      },
    
    onPrepare: () => {

        //console.log("Url del Ambiente de Ejecucion =>", browser.params.url);
        //browser.get(browser.params.url);
        
    },

    
    onComplete: () => {
        //Create html report
        var reporter = require('cucumber-html-reporter');
        var options = {
            theme: 'bootstrap',
            jsonFile: './reports/json-reports/cucumber.json',
            output: './reports/html-reports/cucumber_report.html',
            reportSuiteAsScenarios: true,
            scenarioTimestamp: true,
            launchReport: false,
            metadata: {
                "Test Environment": "Valiun test",
                "Browser": "Chrome",
                "Platform": "Linux",
                "Parallel": "Scenarios",
                "Executed": "Remote"
            },
        };
        reporter.generate(options);
    },

    cucumberOpts: {
        compiler: 'ts:ts-node/register',
        format: [
            'json:reports/json-reports/cucumber.json'
          ],
        require: [
            '../step_definition/*.js',
            '../hooks/*.js'
        ],
        'dry-run': false,
        strict: true,
        tags: '@smoke'
    }

}