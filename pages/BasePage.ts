import {Locator, browser, element, by, protractor, $$, $} from "protractor";
import { SSL_OP_EPHEMERAL_RSA } from 'constants';
import { ExecException } from 'child_process';

export class BasePage {

    private EC = protractor.ExpectedConditions;

    public async getInnerText(webLocator:Locator):Promise<String>{
        const webelement = element(webLocator);
        await browser.wait(protractor.ExpectedConditions.presenceOf(webelement), 3000);
        //console.log("Prueba de consulta de los Elementos",webelement);
        return await webelement.getText();
    }

    public async sendKeysToElement(webLocator: Locator, keys:string):Promise<void>{
        const webelement = element(webLocator);
        await browser.wait(protractor.ExpectedConditions.presenceOf(webelement), 3000);
        await webelement.sendKeys(keys);
    }

    public async clickOnWebElement(webLocator: Locator):Promise<void>{
        const webelement = element(webLocator);
        await browser.wait(protractor.ExpectedConditions.presenceOf(webelement), 3000);
        await webelement.click();
        //browser.wait(browser.sleep(5000));
        
    }


    public async clearWebElement(webLocator: Locator):Promise<void>{
        const webelement = element(webLocator);
        await browser.wait(protractor.ExpectedConditions.presenceOf(webelement), 3000);
        await webelement.clear();
    }

    public async displayedWebElement(webLocator: Locator):Promise<void>{
        const webelement = element(webLocator);
        if (webelement.isDisplayed)
        {
            this.HighLight(webLocator);
            console.log("Objeto presente en la pagina =>", webelement);
        }
        else
        {
            console.log("El siguiente objeto no esta presente en la pagina =>", webelement);
        }
    }

    public async HighLight(webLocator: Locator):Promise<void>{
        const webelement = element(webLocator);
        await browser.wait(protractor.ExpectedConditions.presenceOf(webelement), 60000);
        for (let i = 0; i < 2; i++){
            try{
                await browser.executeScript("return arguments[0].setAttribute('style','background: gray')", webelement);
                await browser.sleep(500);
                await browser.executeScript("arguments[0].setAttribute('style','background:')", webelement);
            }
            catch(e){
                e.MessageUps;
            }

        }
           
    }
    
}