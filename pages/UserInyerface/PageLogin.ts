import { browser, element, by, Locator, protractor, $$, $} from "protractor";
import { BasePage } from "../BasePage";


export class PageLogin extends BasePage {

    public readonly ImgUserInyerface: Locator = by.css("div.logo__icon");
    public readonly TextOne: Locator = by.xpath("//div[@id='app']/div/div/div[2]/p");
    public readonly TextTwo: Locator = by.xpath("//p[2]");
    public readonly BtnNo: Locator = by.xpath("//button");
    public readonly BtnHere: Locator = by.linkText('HERE');

}